package edu.missouristate.util.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class InterceptorHandler implements HandlerInterceptor {
	private int debug = 0;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		if (debug > 0) {
			StringBuffer url = request.getRequestURL();
			String uri = request.getRequestURI();
			System.out.println("PRE-HANDLE! URI: " + uri);
		}
			
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//if (debug > 0)
			//System.out.println("POST-HANDLE (VIEW NOT CREATED YET)!");
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		//if (debug > 0)
		//	System.out.println("AFTER-COMPLETION (VIEW CREATED)!");
	}
}