<!DOCTYPE html>
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>${title}</title>
	<link rel="shortcut icon" href="//www.missouristate.edu/favicon.ico">
	<!-- Bootstrap import -->
	<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>">
	<!-- Project CSS import -->
	<link type="text/css" href="<c:url value='/resources/css/master.css' />" rel="stylesheet" />
</head>
<body>
	<div id="bodyContentTile" class="container">
		<div class="row">
			<div class="col-sm-12">
				You're Logged in!
			</div>
		</div>
	</div>
</body>
</html>