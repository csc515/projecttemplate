<!DOCTYPE html>
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>${title}</title>
	<link rel="shortcut icon" href="//www.missouristate.edu/favicon.ico">
	<!-- Bootstrap import -->
	<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>">
	<!-- Project CSS import -->
	<link type="text/css" href="<c:url value='/resources/css/master.css' />" rel="stylesheet" />
</head>
<body>
	<div id="bodyContentTile" class="container">
		<div class="row">
			<div class="col-sm-12">

				<div class="row mt-4">
					<div class="hidden-xs col-sm-2 col-md-3 col-lg-4"></div>
					<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
				    	<div class="card card-default">
							<div class="card-heading mx-auto">
				        		<header>
									<a href="#">
									  <img class="pt-2" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVPUqKxxvlJOtTpOAAaO359quWCRhxQOU3AA&usqp=CAU" alt="Logo">
									</a>
								</header>
							</div>
							<div class="card-body">
								<div>
									<form id="loginForm" name="loginForm" action="<c:url value='/loginProcess'/>" method="POST">
										<legend>Log in</legend>          
										<div class="form-group ">
											<label class="control-label" for="username">Username</label>
											<input class="form-control" id="username" name="username" type="text" value="" />
										</div>
										<div class="form-group">
											<label class="control-label" for="password">Password</label>
											<input class="form-control" id="password" name="password" type="password" value="">
										</div>
										<div class="form-group text-center">
											<button class="login-btn btn btn-primary" type="submit" name="_eventId_proceed" id="login" onclick="this.childNodes[0].nodeValue='Logging in, please wait...'">Login</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="hidden-xs col-sm-2 col-md-3 col-lg-4"></div>
				</div>
				<style>
					.card-body {
						padding-bottom: 0;
					}
				</style>				
				
			</div>
		</div>
	</div>
</body>
</html>

